


var edad;
var altura;
var peso;       
var imc;
var numero=0;
var nivel;
var tabla=document.getElementById('registro');
function GeneracionNumerosaleatorios()
{
    edad=Math.random()*(99-18)+18;
    edad=edad.toFixed();
    document.getElementById('edad').value=edad;
    altura=Math.random()*(1.5-2.5)+2.5;
    altura=altura.toFixed(1);
    document.getElementById('altura').value=altura;
    peso=Math.random()*(130-20)+20;
    peso=peso.toFixed();
    document.getElementById('peso').value=peso;
}


function calcularIMC()
{
    imc=peso/(altura*altura);
    imc=imc.toFixed(1);
    document.getElementById('imc').value=imc;
    
    if(imc<18.5)
    {     
       
        nivel="Bajo de Peso";
    }
     if(imc>=18.5 && imc<=24.9)
    {
        
        nivel="Peso Saludable";
    }
     if(imc>=25.0 && imc<=29.9)
    {
        
        nivel="Sobre Peso";
    }
    if(imc>=30.0)
    {
        
        nivel="Obesidad";
    }
    document.getElementById('nivel').value=nivel;
}
function registrar(){

    
   
   numero++;
    tabla.innerHTML=tabla.innerHTML+"<br>"+"Numero "+numero+" Edad: "+edad+" Altura: "+altura+" Peso: "+peso+" IMC: "+imc+" Nivel: "+nivel;
}
function borrar()
{
    
    tabla.innerHTML="";
}

var btnCalcular=document.getElementById('calcular');
btnCalcular.addEventListener('click',calcularIMC);
var btnGenerar=document.getElementById('generar');
btnGenerar.addEventListener('click',GeneracionNumerosaleatorios);
var btnRegistrar=document.getElementById('registrar');
btnRegistrar.addEventListener('click',registrar);
var btnBorrar=document.getElementById('limpiar');
btnBorrar.addEventListener('click',borrar)

